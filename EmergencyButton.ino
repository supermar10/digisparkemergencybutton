#include "DigiKeyboard.h"

#define buttonPin 2
#define ledPin 1

int lowCount = 0;

void setup() {
  pinMode(buttonPin, INPUT);
  digitalWrite(buttonPin, HIGH);
  
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, HIGH);
  delay(500);
  digitalWrite(ledPin, LOW);
  
  DigiKeyboard.update();
}

void loop() {
  int button = digitalRead(buttonPin);

  // Not pressed, pull ups activated button connects this pin to GND
  if(button == 1){
    digitalWrite(ledPin, HIGH);
    lowCount = 0;
  }
  else{
    // Very basic debouncing mechanism
    lowCount++;
    if(lowCount == 100){
        digitalWrite(ledPin, LOW);
        DigiKeyboard.sendKeyStroke(KEY_L, MOD_GUI_LEFT);
    }
  }
}
